package com.vazhasapp.assignment11.api

import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

object CountryApiService {
    const val API_ENDPOINT = "/rest/v2/all"
    private const val BASE_URL = "https://restcountries.eu"

    fun apiService(): CountryAPI {
        return Retrofit.Builder()
            .baseUrl(BASE_URL)
            .addConverterFactory(GsonConverterFactory.create())
            .build()
            .create(CountryAPI::class.java)!!
    }
}