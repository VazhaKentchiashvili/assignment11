package com.vazhasapp.assignment11.api

import com.vazhasapp.assignment11.api.CountryApiService.API_ENDPOINT
import com.vazhasapp.assignment11.model.CountryModel
import retrofit2.Response
import retrofit2.http.GET

interface CountryAPI {

    @GET(API_ENDPOINT)
    suspend fun getAllCountries() : Response<List<CountryModel>>
}