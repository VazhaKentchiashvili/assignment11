package com.vazhasapp.assignment11

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log.d
import androidx.lifecycle.ViewModel
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.gson.JsonObject
import com.vazhasapp.assignment11.api.CountryApiService.apiService
import com.vazhasapp.assignment11.databinding.ActivityMainBinding
import com.vazhasapp.assignment11.model.CountryModel
import kotlinx.coroutines.*
import org.json.JSONException

class MainActivity : AppCompatActivity() {

    private lateinit var binding: ActivityMainBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)
    }
}