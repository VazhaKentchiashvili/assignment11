package com.vazhasapp.assignment11.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.vazhasapp.assignment11.api.CountryApiService
import com.vazhasapp.assignment11.model.CountryModel
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class CountryViewModel : ViewModel() {

    private val ioDispatcher: CoroutineDispatcher = Dispatchers.IO

    private var _countriesLiveData = MutableLiveData<List<CountryModel>>().apply {
        mutableListOf<CountryModel>()
    }
    val countriesLiveData: LiveData<List<CountryModel>> = _countriesLiveData

    private var _loadingResponse = MutableLiveData<Boolean>()
    val loadingResponse: LiveData<Boolean> = _loadingResponse

    fun getResponse() {
        CoroutineScope(ioDispatcher).launch {
            getCountriesResponse()
        }
    }

    private suspend fun getCountriesResponse() {
        val result = CountryApiService.apiService().getAllCountries()
        if (result.isSuccessful) {
            _loadingResponse.postValue(true)
            val countries = result.body()
            _countriesLiveData.postValue(countries)
        } else {
            result.code()
        }
        _loadingResponse.postValue(false)
    }
}