package com.vazhasapp.assignment11

import android.net.Uri
import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.ImageView
import androidx.recyclerview.widget.RecyclerView
import com.github.twocoffeesoneteam.glidetovectoryou.GlideToVectorYou
import com.vazhasapp.assignment11.databinding.CountryCardViewBinding
import com.vazhasapp.assignment11.model.CountryModel

class ApiRecyclerAdapter :
    RecyclerView.Adapter<ApiRecyclerAdapter.ApiRecyclerViewHolder>() {

    private var countries = mutableListOf<CountryModel>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ApiRecyclerViewHolder {
        return ApiRecyclerViewHolder(
            CountryCardViewBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            )
        )
    }

    override fun onBindViewHolder(holder: ApiRecyclerViewHolder, position: Int) {
        holder.bind()
    }

    override fun getItemCount() = countries.size

    inner class ApiRecyclerViewHolder(private val binding: CountryCardViewBinding) :
        RecyclerView.ViewHolder(binding.root) {
        private lateinit var currentCountry: CountryModel

        fun bind() {
            currentCountry = countries[adapterPosition]

            binding.tvCountryName.text = currentCountry.name
            binding.tvCountryCapital.text = currentCountry.capital
            binding.tvCountryRegion.text = currentCountry.region
            binding.tvCountryCurrency.text = currentCountry.currencies[0].name

            binding.imCountryFlag.getSvg(currentCountry.flag)
    }
}

    fun setData(countries: List<CountryModel>) {
        this.countries.clear()
        this.countries.addAll(countries)
        notifyDataSetChanged()
    }

    fun clearData() {
        countries.clear()
        notifyDataSetChanged()
    }

    fun ImageView.getSvg(url: String?) {
        GlideToVectorYou
            .init()
            .with(this.context)
            .load(Uri.parse(url), this)
    }
}