package com.vazhasapp.assignment11.model

data class CountryModel(
    val name: String,
    val capital: String,
    val region: String,
    val flag: String,
    val currencies: List<CurrencyModel>,
)
