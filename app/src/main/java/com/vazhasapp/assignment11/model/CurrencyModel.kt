package com.vazhasapp.assignment11.model

data class CurrencyModel(
    val name: String,
    val symbol: String,
)
